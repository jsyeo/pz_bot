require 'discordrb'
require 'dotenv'

Dotenv.load
token = ENV['DISCORD_TOKEN']

bot = Discordrb::Commands::CommandBot.new token: token, prefix: '!'

tmux_session = 'pz'

bot.command :restart do |event|
  event.respond 'Server restarting in 1 min...'
  sleep 60
  `tmux send-keys -t #{tmux_session} 'quit' Enter`
  sleep 30
  `tmux send-keys -t #{tmux_session} 'bash /home/steam/pz_server/start-server.sh -servername gems' Enter`
  'Server restarted... give it a ~30 seconds for it stabilize before connecting to it'
end

bot.command :players do |event|
  `tmux pipe-pane -t #{tmux_session} 'cat >/tmp/capture'`
  `tmux send-keys -t #{tmux_session} 'players' Enter`
  sleep 1
  `tmux pipe-pane -t #{tmux_session}`

  lines = File.read('/tmp/capture').split("\n").map { |l| l.strip }.filter { |l| !l.empty? }
  players = lines.filter { |l| l.start_with? '-' }
  if players.empty?
    'No one is online'
  else
    event.respond "Players connected: (#{players.size})"
    players.join "\n"
  end
end

bot.run

